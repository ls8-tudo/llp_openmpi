# README #

Zurzeit werden die Daten statisch geladen, d.h. die Dateinamen müssen dem Code entnommen werden.

Getting Started:

```
#!bash
sudo apt-get install -y r-base r-base-dev
sudo apt-get install -y openssh-server openssh-client
sudo apt-get install -y openmpi-bin openmpi-doc libopenmpi-dev

wget https://cran.r-project.org/src/contrib/Rmpi_0.6-6.tar.gz
R CMD INSTALL Rmpi_0.6-6.tar.gz

orterun --machinefile <machines1, machines2> Rscript llp.R
```
