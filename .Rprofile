library(utils)
library(stats)
library(datasets)
library(grDevices)
library(graphics)
library(methods)

library(Rmpi)

options(error = quote(
        assign(".mpi.err", FALSE, env = .GlobalEnv)))

if (mpi.comm.size(0) > 1) {
    invisible(mpi.comm.dup(0, 1))
}

if (mpi.comm.rank(0) > 0) { # slave
    options(echo = FALSE)

    mpi.barrier(0)   

    repeat {
	    try(eval(mpi.bcast.cmd(rank = 0, comm = 1)), TRUE)
    }    
    
    if (is.loaded("mpi_comm_disconnect")) {
        mpi.comm.disconnect(1)
    } else {
        mpi.comm.free(1)
    }

    mpi.quit()
}

if (mpi.comm.rank(0) == 0) { # master
    mpi.barrier(0)

    if(mpi.comm.size(0) > 1)
        slave.hostinfo(1)
}

.Last <- function(){
    if (is.loaded("mpi_initialize")) {
        if (mpi.comm.size(1) > 1) {
            mpi.close.Rslaves(comm = 1)
        }
    }

    mpi.quit()
}

